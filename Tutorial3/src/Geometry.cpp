#include "Geometry.h"
#include "aie/Gizmos.h"

#include "Cameras/FlyCamera.h"
#include "Resources/Mesh.h"

#include "imgui.h"

#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"

using namespace GLEngine;

void APIENTRY openglCallbackFunction(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	std::cout << "---------------------opengl-callback-start------------" << std::endl;
	std::cout << "message: " << message << std::endl;
	std::cout << "type: ";
	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		std::cout << "ERROR";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		std::cout << "DEPRECATED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		std::cout << "UNDEFINED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		std::cout << "PORTABILITY";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		std::cout << "PERFORMANCE";
		break;
	case GL_DEBUG_TYPE_OTHER:
		std::cout << "OTHER";
		break;
	}
	std::cout << std::endl;

	std::cout << "id: " << id;
	std::cout << "severity: ";
	switch (severity) {
	case GL_DEBUG_SEVERITY_LOW:
		std::cout << "LOW";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		std::cout << "MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_HIGH:
		std::cout << "HIGH";
		break;
	}
	std::cout << std::endl;
	std::cout << "---------------------opengl-callback-end--------------" << std::endl;
}

void TurnOnDebugLogging()
{
	if (glDebugMessageCallback == nullptr) return;
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(openglCallbackFunction, nullptr);
	GLuint unusedIds = 0;
	glDebugMessageControl(
		GL_DONT_CARE, // source
		GL_DONT_CARE, // type
		GL_DONT_CARE, // severity
		0,
		&unusedIds,
		true);
}

Geometry::Geometry(std::string str, unsigned int uiWidth, unsigned int uiHeight)
	: GLApplication(str, uiWidth, uiHeight)
	, m_pLoader(nullptr)
	, m_pFlyCamera(nullptr)
	, m_bDrawGizmoGrid(true)
{
}

Geometry::~Geometry()
{
	delete m_pCamera;
}

bool Geometry::Startup()
{
	TurnOnDebugLogging();

	glEnable(GL_BLEND);

	m_pLoader = new AssetLoader();


	m_pFlyCamera = new FlyCamera(); m_pFlyCamera->SetInputWindow(m_pWindow);
	m_pCamera = m_pFlyCamera;

	m_pCamera->SetupPerspective(glm::pi<float>() * 0.25f,
		(float)GetScreenWidth() / (float)GetScreenHeight(), 0.1f, 1000.f);

	m_pCamera->LookAt(glm::vec3(10, 10, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	m_pLoader->SetAssetPath("Data");
	m_pFBXBunnyMesh = m_pLoader->LoadAsset<Mesh>("Models/Stanford/Bunny.fbx");
	m_pOBJBunnyMesh = m_pLoader->LoadAsset<Mesh>("Models/Stanford/Bunny.obj");

	m_pFBXBunnyMesh->SetShader("Shaders/default.shader");
	m_pOBJBunnyMesh->SetShader("Shaders/default.shader");

	m_pShader = m_pOBJBunnyMesh->GetShader();

	return true;

}

void Geometry::Shutdown()
{
	m_pFBXBunnyMesh->Unload();
	m_pOBJBunnyMesh->Unload();

	delete m_pLoader;
}

bool Geometry::Update(double dt)
{
	m_pFlyCamera->Update(dt);

	return !(glfwGetKey(m_pWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS);
}

void Geometry::Render()
{
	if(m_bDrawGizmoGrid == true)
		DrawGizmoGrid();

	glm::mat4 fbxModel(1);
	fbxModel[3] = glm::vec4(-5, 0, 0, 1);
	glm::mat4 objModel(1);
	objModel[3] = glm::vec4(5, 0, 0, 1);
	
	m_pShader->SetModelUniform(fbxModel);
	m_pFBXBunnyMesh->Render(m_pCamera);
	m_pShader->SetModelUniform(objModel);
	m_pOBJBunnyMesh->Render(m_pCamera); 


	static float f = 0.0f;
}