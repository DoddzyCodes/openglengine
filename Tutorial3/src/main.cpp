#include "GLApplication.h"

#include <gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include "Geometry.h"

int main()
{
	Geometry app("Geomery Example", 1280, 720);
    
	app.Run();

    return 0;
}