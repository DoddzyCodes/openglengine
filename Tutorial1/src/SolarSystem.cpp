#include "SolarSystem.h"
#include "aie/Gizmos.h"

#include "Cameras/FlyCamera.h"

using namespace GLEngine;

SolarSystem::Planet::Planet()
	: m_localTransform(1)
	, m_planetTransform(1)
	, m_fRotateSpeed(1.0f)
	, m_fRadius(2.0f)
	, m_fOrbitSpeed(0.0f)
	, m_colour(1, 0, 0, 1)
	, m_pParent(nullptr)
{

}

glm::mat4 SolarSystem::Planet::GetGlobalTransform()
{
	glm::mat4 global(1);

	if (m_pParent)
		global = m_pParent->GetGlobalTransform();

	return global * m_localTransform;
}

void SolarSystem::Planet::Update(double dt)
{
	glm::mat4 rot = glm::rotate(m_fRotateSpeed * (float)dt, glm::vec3(0, 1, 0));
	m_planetTransform *= rot;

	rot = glm::rotate(m_fOrbitSpeed * (float)dt, glm::vec3(0, 1, 0));
	m_localTransform = rot * m_localTransform;
}

void SolarSystem::Planet::Render()
{
	glm::mat4 global = GetGlobalTransform() * m_planetTransform;

	glm::vec3 pos(global[3].x, global[3].y, global[3].z);
	glm::mat4 rot = global; rot[3] = glm::vec4(0, 0, 0, 1);
	Gizmos::addSphere(pos, m_fRadius, 10, 10, m_colour, &rot);
}

SolarSystem::SolarSystem(std::string str, unsigned int uiWidth, unsigned int uiHeight)
	: GLApplication(str, uiWidth, uiHeight)
{
	sun.SetRotateSpeed(0.1f);
	sun.SetColour(glm::vec4(1, 1, 0, 1));

	p1.SetParentPlanet(&sun);
	glm::mat4 local(1);
	local[3] = glm::vec4(5, 0, 0, 1);
	p1.SetLocalTransform(local);
	p1.SetRotateSpeed(2.0f);
	p1.SetRadius(0.5f);
	p1.SetOrbitSpeed(1.5f);



	p2.SetParentPlanet(&sun);
	p2.SetColour(glm::vec4(0, 1, 0, 1));
	local[3] = glm::vec4(10, 0, 0, 1);
	p2.SetLocalTransform(local);
	p2.SetRotateSpeed(1.0f);
	p2.SetRadius(0.6f);
	p2.SetOrbitSpeed(1.2f);

	p2Moon1.SetParentPlanet(&p2);
	p2Moon1.SetColour(glm::vec4(0.2, 0.2, 0.2, 1));
	
	local[3] = glm::vec4(1.4, 0, 0, 1);
	p2Moon1.SetLocalTransform(local);
	p2Moon1.SetRotateSpeed(1.0f);
	p2Moon1.SetRadius(0.1f);
	p2Moon1.SetOrbitSpeed(2.0f);


	p3.SetParentPlanet(&sun);
	p3.SetColour(glm::vec4(0, 0, 1, 1));
	local[3] = glm::vec4(8, 0, 0, 1);
	p3.SetLocalTransform(local);
	p3.SetRotateSpeed(1.0f);
	p3.SetRadius(0.4f);
	p3.SetOrbitSpeed(1.3f);



}

SolarSystem::~SolarSystem()
{

}

bool SolarSystem::Startup()
{
    //Create FlyCamera, but we won't store it (used for tutorial 2)
    FlyCamera* pFlyCamera = new FlyCamera(); pFlyCamera->SetInputWindow(m_pWindow);
    m_pCamera = pFlyCamera;

    m_pCamera->SetupPerspective(glm::pi<float>() * 0.25f,
							(float)GetScreenWidth() / (float)GetScreenHeight(), 0.1f, 1000.f);
                            
    m_pCamera->LookAt(glm::vec3(10, 10, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));


	return true;
}

void SolarSystem::Shutdown()
{
}

bool SolarSystem::Update(double dt)
{
	sun.Update(dt);
	p1.Update(dt);
	p2.Update(dt);
	p2Moon1.Update(dt);
	p3.Update(dt);

	return true;
}

void SolarSystem::Render()
{
	DrawGizmoGrid();

	sun.Render();
	p1.Render();
	p2.Render();
	p2Moon1.Render();
	p3.Render();
}

