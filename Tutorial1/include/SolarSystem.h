#pragma once

#include "GLApplication.h"

class SolarSystem : public GLEngine::GLApplication
{
private:
	class Planet
	{
	public:
		Planet();
		~Planet() {};

		void SetParentPlanet(Planet* pPlanet) { m_pParent = pPlanet; }

		void SetLocalTransform(const glm::mat4& local) { m_localTransform = local; }
		void SetAttachTransform(const glm::mat4& attach) { m_planetTransform = attach; }

		void SetRotateSpeed(float fRotateSpeed) { m_fRotateSpeed = fRotateSpeed; }
		void SetOrbitSpeed(float fOrbitSpeed) { m_fOrbitSpeed = fOrbitSpeed; }
		void SetRadius(float fRadius) { m_fRadius = fRadius; }
		
		void SetColour(const glm::vec4& colour){ m_colour = colour;  }

		glm::mat4 GetGlobalTransform();
		
		void Update(double dt);
		void Render();

	private:
		Planet* m_pParent;
		glm::mat4 m_localTransform;
		glm::mat4 m_planetTransform;

		float m_fRadius;
		float m_fRotateSpeed;
		float m_fOrbitSpeed;

		glm::vec4 m_colour;
	};


public:
	SolarSystem(std::string str, unsigned int uiWidth, unsigned int uiHeight);
	~SolarSystem();

	virtual bool Startup() override;

	virtual void Shutdown() override;

	virtual bool Update(double dt) override;

	virtual void Render() override;

private:
	Planet sun;
	Planet p1;
	Planet p2;
	Planet p3;

	Planet p2Moon1;
};