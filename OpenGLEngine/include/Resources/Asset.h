#pragma once

#include <string>
namespace GLEngine
{
	class IAssetTracker;
	class Asset
	{
	public:
		Asset(IAssetTracker* pAssetTracker, std::string& strFilePath);
		virtual ~Asset() {};


		Asset() = delete;
		Asset(Asset&& other) = delete;
		Asset& operator=(Asset&& other) = delete;
		Asset(const Asset& other) = delete; //No copy
		Asset& operator=(Asset& other) = delete;

		void Bind();
		void Unbind();

		bool BindIfNeeded();
		void UnbindIfNeeded(bool b);

		void Unload();

		std::string GetAssetID() const;
	protected:
		virtual void onBind() = 0;
		virtual void onUnbind() = 0;

		IAssetTracker* m_pOwningAssetTracker;
		std::string m_strFilePath;

		bool		m_bInitialized;
		bool		m_bCurrentBound;
	};
};