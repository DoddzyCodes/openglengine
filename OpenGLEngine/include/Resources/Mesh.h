#pragma once

#include "Resources/Asset.h"
#include "Rendering/RenderData.h"
#include <tiny_obj_loader.h>

#include <vector>
#include <assert.h>

class FBXFile;
class FBXMeshNode;

namespace GLEngine
{

	class BaseCamera;
	class Shader;
	class Material;
	class Mesh : public Asset
	{
	public:
		Mesh(IAssetTracker* pAssetTracker, std::string& strFilePath);
		~Mesh();
		
		void Render(BaseCamera* pCamera, bool bBindShaderUniforms = true);

		void SetShader(std::string strShaderPath);
		Shader* GetShader() const { return m_pMeshShader; }
	private:

		void onBind() override;
		void onUnbind() override;

		void LoadFromOBJ(std::string path);
		void LoadFromFBX(std::string path);

#ifdef FBX_SUPPORTED
		void BuildRenderDataFromLoaderNode(RenderData** pRenderData, FBXMeshNode* pMesh);
#endif
		void BuildRenderDataFromLoaderNode(RenderData** pRenderData, tinyobj::shape_t* pMesh);

		template<typename T>
		void BuildMaterialFromLoaderNode(Material** pMaterial, T* pLoaderMaterial, std::string additionalPath = "")
		{
			assert(pLoaderMaterial);

			*pMaterial = m_pOwningAssetTracker->GetAssetLoader()->LoadAsset<Material>(pLoaderMaterial->name, false);
			if (!(*pMaterial)->IsInitialized()) (*pMaterial)->InitializeFromLoaderMaterial(pLoaderMaterial, additionalPath);
		}

		void ValidateMesh();
		std::string GetPathDirectory(std::string path);
		RenderData* m_pRenderObject;
		Shader*		m_pMeshShader;
		Material*	m_pMeshMaterial;

		std::vector<Mesh*> m_internalMeshes;

		bool		m_bValidated;
	};
};