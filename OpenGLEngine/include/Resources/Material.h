#pragma once

#include "Asset.h"
#include "tiny_obj_loader.h"
struct FBXMaterial;


namespace GLEngine
{

class Texture;
class Material : public Asset
{
public:
	enum TextureSlots
	{
		DiffuseTexture = 0,
		AmbientTexture,
		GlowTexture,
		SpecularTexture,
		GlossTexture,
		NormalTexture,
		AlphaTexture,
		DisplacementTexture,

		TextureTypes_Count
	};

	Material(IAssetTracker* pAssetTracker, std::string& strMaterialName);
	~Material();

	bool HasTexture(TextureSlots eSlot) { return m_pTextures[eSlot] != nullptr; }

	bool IsInitialized() { return m_bInitialized; }

	template<typename T>
	void InitializeFromLoaderMaterial(T* pMaterial, std::string strAdditionalPath)
	{
		LoadIfExists(TextureSlots::DiffuseTexture, pMaterial, TextureSlots::DiffuseTexture, strAdditionalPath);
		LoadIfExists(TextureSlots::AmbientTexture, pMaterial, TextureSlots::AmbientTexture, strAdditionalPath);
		LoadIfExists(TextureSlots::GlowTexture, pMaterial, TextureSlots::GlowTexture, strAdditionalPath);
		LoadIfExists(TextureSlots::SpecularTexture, pMaterial, TextureSlots::SpecularTexture, strAdditionalPath);
		LoadIfExists(TextureSlots::GlossTexture, pMaterial, TextureSlots::GlossTexture, strAdditionalPath);
		LoadIfExists(TextureSlots::NormalTexture, pMaterial, TextureSlots::NormalTexture, strAdditionalPath);
		LoadIfExists(TextureSlots::AlphaTexture, pMaterial, TextureSlots::AlphaTexture, strAdditionalPath);
		LoadIfExists(TextureSlots::DisplacementTexture, pMaterial, TextureSlots::DisplacementTexture, strAdditionalPath);

		m_bInitialized = true;
		DebugConsole::Get()->Message(DebugConsole::General, "Material", "Finished initializing material: " + m_pOwningAssetTracker->GetAssetID());
	}
private:
#ifdef FBX_SUPPORTED
	void LoadIfExists(TextureSlots slot, FBXMaterial* pMaterial, unsigned int FBXTextureType, std::string strAdditionalPath);
#endif
	void LoadIfExists(TextureSlots slot, tinyobj::material_t* pMaterial, unsigned int FBXTextureType, std::string strAdditionalPath);


	void onBind() override;
	void onUnbind() override;

	Texture* m_pTextures[TextureSlots::TextureTypes_Count];
};

};