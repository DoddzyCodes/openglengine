#pragma once

#include <vector>
#include <string>
#include <unordered_map>
#include <memory>

#include "AssetLoading/AssetTracker.h"

#include "Resources/Mesh.h"
#include "Resources/Shader.h"
#include "Resources/Texture.h"
#include "Resources/Material.h"

#include <iostream>
#include <assert.h>

#define ALLOW_ASSET_LOADING(className) \
	template<> \
	className* AssetLoader::CreateAsset<className>(AssetTracker<className>* pTracker, std::string fullPath) \
	{ \
		className* pResource = new className(pTracker, fullPath); \
		pTracker->BindAsset(pResource); \
		return pResource; \
	}

namespace GLEngine
{

class AssetLoader
{
public:
	AssetLoader();
	~AssetLoader();
	AssetLoader(AssetLoader&& other);
	AssetLoader& operator=(AssetLoader&& other);

	AssetLoader(const AssetLoader& other)		= delete; //No copy
	AssetLoader& operator=(AssetLoader& other)	= delete;
	
	void SetAssetPath(std::string assetPath);
	std::string GetAssetPath() const { return m_assetPath; }

	template<typename T>
	T* LoadAsset(std::string loadPath, bool bAppendAssetPath = true);
	void RemoveTrackedAsset(IAssetTracker* asset);
private:

	template<typename T>
	T* CreateAsset(AssetTracker<T>* pTracker, std::string fullPath)
	{
		assert(false && "You can not load an asset of that type");
		return nullptr;
	}
	ALLOW_ASSET_LOADING(Mesh);
	ALLOW_ASSET_LOADING(Shader);
	ALLOW_ASSET_LOADING(Texture);
	ALLOW_ASSET_LOADING(Material);

	std::string m_assetPath;
	std::unordered_map<std::string, IAssetTracker*> m_trackedAssets;
};

template<typename T>
T*
AssetLoader::LoadAsset(std::string loadPath,bool bAppendAssetPath)
{

	std::string fullPath;
	if (bAppendAssetPath)
		fullPath = m_assetPath + "/" + loadPath;
	else
		fullPath = loadPath;

	auto it = m_trackedAssets.find(fullPath);

	if (it != m_trackedAssets.end())
	{
		it->second->IncRefCount();
		return static_cast<AssetTracker<T>*>(it->second)->GetAsset();
	}


	//Need to create a new resource
	AssetTracker<T>* pResourceAsset = new AssetTracker<T>(this, fullPath);
	CreateAsset<T>(pResourceAsset, fullPath);
	pResourceAsset->IncRefCount();

	m_trackedAssets[fullPath] = pResourceAsset;

	return pResourceAsset->GetAsset();
}




};