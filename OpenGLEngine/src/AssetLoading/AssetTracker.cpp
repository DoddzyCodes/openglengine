#include "AssetLoading/AssetTracker.h"

#include "AssetLoading/AssetLoader.h"

namespace GLEngine
{

	IAssetTracker::IAssetTracker(AssetLoader* pLoader, std::string id)
		: m_pLoader(pLoader)
		, m_assetID(id)
		, m_refCount(0)

	{

	}

	IAssetTracker::IAssetTracker(IAssetTracker&& other)
	{
		m_pLoader = std::move(other.m_pLoader);
		m_refCount = std::move(other.m_refCount);
		m_assetID = std::move(other.m_assetID);
	}

	IAssetTracker& IAssetTracker::operator=(IAssetTracker&& other)
	{
		m_pLoader = std::move(other.m_pLoader);
		m_refCount = std::move(other.m_refCount);
		m_assetID = std::move(other.m_assetID);

		return *this;
	}
};