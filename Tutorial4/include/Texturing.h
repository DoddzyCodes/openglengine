#pragma once

#include "GLApplication.h"

#include "AssetLoading/AssetLoader.h"
#include "Resources/Asset.h"

namespace GLEngine
{
	class FlyCamera;
	class Mesh;
};

class Texturing : public GLEngine::GLApplication
{
public:
	Texturing(std::string str, unsigned int uiWidth, unsigned int uiHeight);
	~Texturing();

	bool Startup() override;

	void Shutdown() override;

	bool Update(double dt) override;

	void Render() override;

private:
    GLEngine::FlyCamera*	m_pFlyCamera;
	GLEngine::AssetLoader*	m_pLoader;

	bool m_bDrawGizmoGrid;

	GLEngine::Mesh* m_pSpear;
};